from django.shortcuts import render
from django.shortcuts import redirect
import pyrebase


config = {
    'apiKey': "AIzaSyBIrUQZgsFI1CNPUF1J3f9AZGmmPfSVuOw",
    'authDomain': "appli-jury-certification.firebaseapp.com",
    'databaseURL': "https://appli-jury-certification.firebaseio.com",
    'projectId': "appli-jury-certification",
    'storageBucket': "appli-jury-certification.appspot.com",
    'messagingSenderId': "521996484265",
    'appId': "1:521996484265:web:a57fcb409e4cd0727433b8",
    'measurementId': "G-CM8XSJDP15"
}

firebase = pyrebase.initialize_app(config)
auth = firebase.auth()
db = firebase.database()


def home(request):
    eleves = db.child('eleve').get()
    dataListe = []
    for liste in eleves.each():
        key = liste.key()
        student = liste.val()
        result = {'key': key, 'student': student}
        dataListe.append(result)
    return render(request, "home.html", {'studentList': dataListe})


# def testBDD(request):
#     data = {"msg": 'Hello test'}
#     db.child("todo").push(data)


def addStudent(request):
    name = request.POST.get('student')
    eleves = db.child('eleve').get()
    loop = 1
    for liste in eleves.each():
        loop += 1

    data = {'name': name, 'id': loop}
    db.child('eleve').push(data)
    return redirect('/')


def eval(request):
    keyId = request.POST.get('keyId')
    studentId = request.POST.get('id')
    studentName = request.POST.get('name')
    competences = db.child('competence').get()
    compListe = []
    for liste in competences.each():
        key = liste.key()
        competence = liste.val()
        result = {'key': key, 'competence': competence}
        compListe.append(result)
    data = {'studentID': studentId,
            'studentName': studentName,
            'compListe': compListe,
            'keyId':keyId}
    return render(request, 'eval.html', {'data': data})


def sendBDD(request):
    data = request.POST.items()

    dataTab = []
    for cle, valeur in data:
        result = {cle: valeur}
        dataTab.append(result)
    keyId = dataTab[12].get('keyId')

    dataBDD = {
        'valComp1': dataTab[2].get('comp_1'),
        'comComp1': dataTab[3].get('comp_1_com'),
        'valComp2': dataTab[4].get('comp_2'),
        'comComp2': dataTab[5].get('comp_2_com'),
        'valComp3': dataTab[6].get('comp_3'),
        'comComp3': dataTab[7].get('comp_3_com'),
        'valComp4': dataTab[8].get('comp_4'),
        'comComp4': dataTab[9].get('comp_4_com'),
        'valComp5': dataTab[10].get('comp_5'),
        'comComp5': dataTab[11].get('comp_5_com'),
    }

    db.child('eleve').child(keyId).child('comp').update(dataBDD)
    # print(eleves)

    return redirect('/')
